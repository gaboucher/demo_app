from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


# Create your models here.
class Animal(models.Model):
    name = models.CharField(max_length=250)
    nb_leg = models.PositiveIntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(1000)])

    def __str__(self):
        return self.name + ' ' + self.make_sound()

    def make_sound(self):
        return 'Default make_sound'


class Bird(Animal):
    def make_sound(self):
        return 'Tweet'

    class Meta:
        proxy = True

class Cat(Animal):
    def make_sound(self):
        return 'Meow'

    class Meta:
        proxy = True

class Dog(Animal):
    def make_sound(self):
        return 'Woof'

    class Meta:
        proxy = True