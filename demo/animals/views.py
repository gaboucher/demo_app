from django.http import HttpResponse
from .models import Animal

from django.template import loader


def index(request):
    animal_list_sorted = Animal.objects.all()
    template = loader.get_template('index.html')
    context = {
        'animal_list': animal_list_sorted,
    }
    return HttpResponse(template.render(context, request))
